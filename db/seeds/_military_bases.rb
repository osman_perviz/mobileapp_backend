#encoding: utf-8
MilitaryBase.delete_all

bases = ["Aachen","Ahlen","Albersdorf","Alt Duvenstedt","Altenhof","Altenstadt","Amberg","Andernach","Ansbach","Appen","Arnsberg","Aschau a. Inn","Augustdorf","Aurich",
         "Bad Aibling","Bad Bergzabern","Baden-Baden","Bad Frankenhausen","Bad Kreuznach","Bad Reichenhall","Bad Salzungen","Bad Sülze","Bamberg","Bargum","Baumholder",
         "Bayerisch Eisenstein","Beelitz","Beeskow","Berlin","Bielefeld","Birkenfeld (Nahe)","Bischofswiesen","Blankenburg (Harz)","Bochum","Bogen","Bonn","Boostedt",
         "Borkum","Brakel","Bramstedtlund","Braunschweig","Brekendorf","Bremen","Bremerhaven","Bremervörde","Bruchsal","Brück","Büchel","Bückeburg","Bünsdorf","Burg (bei Magdeburg)",
         "Calden","Calw","Cammin-Prangendorf","Celle","Cham","Cochem","Cölpin","Cottbus","Darmstadt","Daun","Deggendorf","Delitzsch","Delmenhorst","Diepholz","Diez","Dillingen a. d. Donau",
         "Doberlug-Kirchhain","Döbern","Donaueschingen","Donauwörth","Dornstadt","Dorsten","Dortmund","Dresden","Dunningen","Düsseldorf","Eckernförde","Egelsbach","Eggesin","Ellwangen (Jagst)",
         "Elpersbüttel","Emden","Erding","Erfurt","Erndtebrück","Eschweiler","Euskirchen","Eutin","Faßberg","Fehmarn","Feldafing","Feldkirchen (Niederbayern)","Flensburg","Flintbek",
         "Frankenberg (Eder)","Frankenberg/Sa.","Frankfurt (Oder)","Frankfurt am Main","Freiburg in Breisgau","Freisen","Freising","Freyung","Friedrichsdorf","Friedrichshafen","Fritzlar",
         "Fürstenfeldbruck","Fürth","Füssen","Garching b. München","Hansestadt Gardelegen (Letzlingen)","Garmisch-Partenkirchen","Geilenkirchen","Gelnhausen","Gelsdorf","Gera","Germersheim",
         "Gerolstein","Giesen","Gillenfeld","Gleina","Glücksburg (Ostsee)","Gnoien","Golchen","Gotha","Göttingen","Grafenwöhr","Greding","Gronau (Westfalen)","Hagen","Hagenow","Halberstadt",
         "Halle (Saale)","Hamburg","Hammelburg","Hannover","Hardheim","Havelberg","Heide","Heidelberg","Helgoland","Herford","Hilden","Hilscheid","Höchstberg","Hof","Hohn",
         "Holzminden","Homberg (Efze)","Höxter","Hürth","Husum","Idar-Oberstein","Immendingen","Immenstaad am Bodensee","Ingolstadt","Itzehoe","Jagel","Jena","Jülich","Kaiserslautern",
         "Kalkar","Kalkhorst","Karlsruhe","Kassel","Kastellaun","Kaufbeuren","Kempten (Allgäu)","Kerpen","Kiel","Kleinaitingen","Klietz","Koblenz","Köln","Königsbrück","Königstein (Sächsische Schweiz)",
         "Königswinter","Konstanz","Kramerhof","Kronshagen","Kropp","Kümmersbruck","Laage","Laboe","Ladelund","Lahnstein","Landsberg am Lech","Langen (Landkreis Cuxhaven)","Langen (Landkreis Offenbach)",
         "Lauda-Königshofen","Laupheim","Lebach","Leer (Ostfriesland)","Leipzig","Lindenberg i. Allgäu","Lohheide","Lorup","Ludwigsfelde","Lüneburg","Magdeburg","Maintal","Mainz","Manching","Mannheim",
         "Marienbaum","Marienberg","Märkische Heide","Marlow","Mayen","Mechernich","Meppen","Merzig","Meßstetten","Minden","Mittenwald","Möckern","Mockrehna","Möchnechgladbach","Mühlhausen/Thüringen",
         "Müllheim","München","Münchsmünster","Münster","Munster","Murnau a. Staffelsee","Naumburg (Saale)","Neckarzimmern","Neubiberg","Neubrandenburg","Neuburg a.d. Donau","Neuharlingersiel","Neumünster",
         "Neuss","Neustadt am Rübenberge","Neustadt in Holstein","Niederstetten","Nienburg/Weser","Nonnweiler","Nordholz","Nordhorn","Nörvenich","Nürnberg","Oberammergau","Oberarnbach","Oberkochen",
         "Oberndorf am Neckar","Oberschöngau","Oberursel (Taunus)","Oberviechtach","Ochtrup","Offenbach am Main","Oldenburg (Oldenburg)","Oldenburg in Holstein","Osterheide","Osterholz-Scharmbeck","Ottobrunn",
         "Paderborn","Panker","Penzing","Perl","Pirmasens","Pforzheim","Pfreimd","Pfullendorf","Pfungstadt","Pinnow","Plön","Pöcking","Porta Westfalica","Potsdam","Prenzlau","Putgarten","Quakenbrück",
         "Ramstein-Miesenbach","Ravensburg","Rechlin","Recklinghausen","Regen","Regensburg","Remscheid","Rennerod","Rheinbach","Rheine","Roding","Rostock","Rotenburg an der Fulda","Rotenburg (Wümme)",
         "Roth","Röthenbach a.d. Pegnitz","Saarbrücken","Saarlouis","Sanitz","Sankt Augustin","Sankt Wendel","Saterland","Schleswig","Schneizlreuth","Schönefeld (b.Berlin)","Schönewalde","Schortens",
         "Schrobenhausen","Schwäbisch Gmünd","Schwanewede","Schwarzenbach a. Wald","Schwarzenborn","Schwedeneck",
         "Schwerin","Schwielowsee","Seedorf (bei Zeven)","Seeth","Sendenhorst","Setzingen","Siegburg","Siegen","Sigmaringen","Sondershausen","Sonthofen","Speyer","Stade","Stadtallendorf","Stadum",
         "Stavenhagen","Stetten am kalten Markt","Stockach","Stolberg (Rheinland)","Storkow (Mark)","Straelen","Stralsund","Strausberg","Stuttgart","Suhl","Swisttal","Teltow","Todtnau","Torgelow","Traunstein",
         "Trier","Trollenhagen","Überlingen","Uedem","Ulm","Ulmen-Vorpochten","Ummendorf (bei Biberach)","Unna","Unterlüß","Untermeitingen","Unterschleißheim","Utzedel","Varel","Veitshöchheim","Viereck",
         "Visselhövede","Volkach","Walldüm","Walsrode","Waren (Mürlitz)","Warendorf","Wedel (Holstein)","Weener","Weiden i. d. Opf.","Weinheim","Weißenfels","Weißkeißel","Wendelstein","Wesel",
         "Weßling","Wester-Ohrstedt","Westerstede","Wetzlar","Wiesbaden","Wildflecken","Wilhelmshaven","Wittmund","Wolgast","Wunstorf","Würzburg","Zeithain","Zetel","Zweibrücken"]


bases.each do |base|
  MilitaryBase.create(:city=> base)
end

