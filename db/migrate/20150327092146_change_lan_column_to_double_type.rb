class ChangeLanColumnToDoubleType < ActiveRecord::Migration
  def up
    remove_column :users,:lan
    add_column :users,:lon,:decimal,{:precision=>10, :scale=>6}
  end

  def down

  end
end
