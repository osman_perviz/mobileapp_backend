class ChangeColumnMilitaryBaseToMilitaryBaseId < ActiveRecord::Migration
  def up
    rename_column :users,:military_base,:military_base_id
  end

  def down
    rename_column :users,:military_base_id,:military_base
  end
end
