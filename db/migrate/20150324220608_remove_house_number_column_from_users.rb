class RemoveHouseNumberColumnFromUsers < ActiveRecord::Migration
  def up
    remove_column :users,:house_number
  end

  def down
    add_column :users,:house_number,:string
  end
end
