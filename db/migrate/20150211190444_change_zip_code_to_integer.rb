class ChangeZipCodeToInteger < ActiveRecord::Migration
  def up
    change_column :users,:zip_code,:integer
  end
  def down
    change_column :users,:zip_code,:string
  end
end
