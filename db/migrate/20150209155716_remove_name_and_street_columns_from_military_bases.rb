class RemoveNameAndStreetColumnsFromMilitaryBases < ActiveRecord::Migration
  def up
    remove_columns :military_bases,:street , :name
  end
  def down
    add_column :military_bases,:street, :name
  end
end
