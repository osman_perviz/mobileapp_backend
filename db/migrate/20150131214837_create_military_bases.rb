class CreateMilitaryBases < ActiveRecord::Migration
  def change
    create_table :military_bases do |t|
      t.string :name
      t.string :city
      t.string :street

      t.timestamps
    end
  end
end
