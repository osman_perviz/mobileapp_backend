class AddHouseNumberToUsersTable < ActiveRecord::Migration
  def up
    add_column :users,:house_number,:integer
  end

  def down
    remove_column :users,:house_number
  end
end
