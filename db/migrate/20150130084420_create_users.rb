class CreateUsers < ActiveRecord::Migration
  def up
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :password
      t.integer :military_base
      t.boolean :account,:default => false
      t.boolean :driver,:default => false
      t.string :street
      t.string :city
      t.string :auth_token

      t.timestamps
    end

    def down
      drop_table :users
    end
  end
end
