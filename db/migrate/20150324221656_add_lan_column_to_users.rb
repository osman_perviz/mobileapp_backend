class AddLanColumnToUsers < ActiveRecord::Migration
  def up
    add_column :users,:lan,:string
  end

  def down
    remove_column :users,:lan
  end
end
