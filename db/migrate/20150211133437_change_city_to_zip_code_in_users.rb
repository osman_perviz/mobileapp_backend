class ChangeCityToZipCodeInUsers < ActiveRecord::Migration
  def up
    rename_column :users,:city,:zip_code
  end
end
