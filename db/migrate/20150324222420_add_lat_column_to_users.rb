class AddLatColumnToUsers < ActiveRecord::Migration
  def up
    add_column :users,:lat,:string
  end

  def down
    remove_column :users,:lat
  end

end
