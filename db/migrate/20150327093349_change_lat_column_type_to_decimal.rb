class ChangeLatColumnTypeToDecimal < ActiveRecord::Migration
  def up
    change_column :users,:lat,:decimal,{:precision=>10, :scale=>6}
  end
end
