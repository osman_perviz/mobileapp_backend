    json.id @user.id
    json.email @user.email
    json.account @user.account
    json.driver @user.driver
    json.street @user.street
    json.zip_code @user.zip_code
    json.tel_number @user.tel_number
    json.name @user.name
    json.lon  @user.lon
    json.lat  @user.lat
    json.transaction_id @user.transaction_id
    json.military_base_id @user.military_base_id

