# encoding: utf-8
class UserMailer < ActionMailer::Base

  default from: "bw-fahrgemeinschaft-lenz@web.de"

  def signup_confirmation(user)
    @user = user

    mail to: user.email,subject:"Bw Fahrgemeinschaft - Willkommen"
  end

  def reset_password(email)

    @email = email


    mail to: email,subject:"Bw Fahrgemeinschaft - Passwort zurücksetzen"
  end

  def reset_password_send(email,pass)
    @email = email
    @pass = pass

    mail to: email , subject: "Bw Fahrgemeinschaft - Neues Passwort"
  end

end
