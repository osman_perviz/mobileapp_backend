class User < ActiveRecord::Base
  has_secure_password
  has_many :tokens
  belongs_to :military_base



  VALID_EMAIL = /\b[A-Z0-9._%a-z\-]+@(?:[A-Z0-9a-z\-]+\.)+[A-Za-z]{2,4}\z/
  validates :email,
            :presence => true,
            :format => VALID_EMAIL
            #:uniqueness => true ---ej dominic this is for email uniqueness :)

  validates :lat,
            allow_nil: true,
            numericality: { greater_than_or_equal_to:  -90, less_than_or_equal_to:  90 }
  validates :lon,
            allow_nil: true,
            numericality: { greater_than_or_equal_to:  -180, less_than_or_equal_to:  180 }

scope :premium_with_address,->{where(:account=>true).where().not(:street=>nil)}


  private

  def self.decrypting_key
    return 'ecd07aac858fb6412acd44554e3049072d3fe1fae40845f493cf3fe2833df8a9'
  end

end
