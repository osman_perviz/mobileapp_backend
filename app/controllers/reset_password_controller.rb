class ResetPasswordController < ApplicationController
  skip_before_action :restrict_access
  def new
    unless params[:email].present?
      flash[:notice] = "Mail is required"
      return false
    end
    @user = User.where(:email => params[:email]).first
    new_pass = random_string
    if @user && @user.update_attribute(:password,new_pass)
        begin
          UserMailer.reset_password_send(@user.email,new_pass).deliver
          flash[:notice] = "Password succesfully reset"
        rescue => ex
          logger.error ex.message
        end
    else
      flash[:notice] = "User does not exist"
    end
  end


  private


  def random_string(length=6)
      (0..10).sort_by {rand}[0,length].join
  end

end
