class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  before_action :restrict_access

 helper_method :current_user

  def current_user
    authenticate_with_http_token do |token,options|
      token =  Token.find_by(token:token)
      return token.user
    end
  end

  def restrict_access
    authenticate_token || render_unauthorize
  end


  def authenticate_token
    authenticate_with_http_token do |token,options|
      Token.find_by(token:token)
    end
  end

  def render_unauthorize
    self.headers['WWW-Authenticate'] = 'Token realm = "Application"'
    render json:{message:"You Are Not Logged in!"},status: 401
  end
end
