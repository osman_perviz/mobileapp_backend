class Api::V1::MilitaryBasesController < ApplicationController
  respond_to :json
  skip_before_action :restrict_access

  def index
    @bases = MilitaryBase.all
    respond_with @bases ,status: 200
  end

  def show
    @base = MilitaryBase.find(params[:id])
    respond_with @base,status:200

  end
end
