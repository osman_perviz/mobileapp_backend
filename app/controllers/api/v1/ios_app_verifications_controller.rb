class Api::V1::IosAppVerificationsController < ApplicationController

  def verify_receipt
    receipt_data = params[:receipt][:receipt_data]
    receipt = Itunes::Receipt.verify! receipt_data ,:allow_sandbox_receipt

    if verify_receipt_return_data(receipt)
      if receipt.original.present?
        current_user.update_attributes(:account=>true,:transaction_id=>receipt.original.transaction_id)
        render :json => {:status => "ok"}
      else
        current_user.update_attributes(:account=>true,:transaction_id=>receipt.transaction_id)
        render :json => {:status => "ok"}
      end
    else
      render json:{message:"Not valid transaction"},status: 402
    end

    rescue StandardError => e
      render :json => {:status => "error", :message => e.message}, :status => 400

  end

  private

  def receipt_params
    params.require(:receipt).permit(:receipt_data)
  end

  def verify_receipt_return_data(receipt)
    if  receipt.bid == 'com.dndapps.bwfahrgemeinschaft.test' && receipt.product_id == 'premium' && verify_transaction(receipt)
       true
    else
       false
    end

  end

  def verify_transaction(receipt)
    if receipt.original.present?
      transaction_id = receipt.original.transaction_id
    else
      transaction_id = receipt.transaction_id
    end

    verify_transaction_id(transaction_id)
  end

  def verify_transaction_id(receipt)
    check_transaction = User.where(:transaction_id => receipt).first
    check_transaction.nil?
  end

end
