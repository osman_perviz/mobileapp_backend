class Api::V1::AccessController < ApplicationController
  respond_to :json
  skip_before_action :restrict_access,only: :create


  def create
    @user = User.where(:email=>params[:access][:email]).first
    decrypted_password = AESCrypt.decrypt(params[:access][:password],User.decrypting_key) if params[:access][:password]
    if @user && @user.authenticate(decrypted_password)
      token = generate_new_token
      # @user.update_attributes(:auth_token => token)
      Token.create(:user_id => @user.id , :token => token)
       respond_with @user,status: 200
    else
      render json: {message:"Bad Credential"},status: 401
    end
  end

  def destroy
    user = User.find(params[:id])
    if user
     token =  request.headers['Authorization']
     token = token.sub("Token token=",'')
     tokenObj = Token.where(token: token).first
     tokenObj.delete
     render json:{message:"Successfully Logged Out"},status: :ok
    else
      render json: {error:user.errors},status: 404
    end
  end

  private

  def generate_new_token
    SecureRandom.uuid.gsub(/\-/,'')
  end

end
