class Api::V1::UsersController < ApplicationController

  respond_to :json
  skip_before_action :restrict_access,only: [:index,:create,:reset_password]
  before_action :ownership_filter,only: [:update,:destroy,:show,:change_password]

  def index
   @users = User.premium_with_address
   respond_with @users
  end

  def create
    decrypt_pass = AESCrypt.decrypt(params[:user][:password],User.decrypting_key) if params[:user][:password].present?
    decrypt_pass_confirm = AESCrypt.decrypt(params[:user][:password_confirmation],User.decrypting_key) if params[:user][:password_confirmation].present?

    email = params[:user][:email]

    @user = User.where(:email=> email).first

    if @user
      return render json: {message:"Email address is already in use."},status: 402
    end

    @user = User.new
    @user.email = email
    @user.password = decrypt_pass
    @user.password_confirmation = decrypt_pass_confirm

    if @user.save
      begin
        UserMailer.signup_confirmation(@user).deliver
        respond_with @user,status: 200
      rescue => ex
        logger.error ex.message
      end
    else
      render json: {message:@user.errors},status: 422
    end
  end

  def show
    @user = User.find(params[:id])
    respond_with @user ,status: :ok
  end


  def update
    @user = User.find(params[:id])

    if @user.update_attributes(user_params)
      respond_with @user, status: 200
    else
      render json: {message:@user.errors},status: 404
    end
  end

  def destroy
     user = User.find(params[:id])
     user.destroy
     render nothing: true , status: 200
  end

  def change_password
      user = User.find(params[:id])
      old_password = AESCrypt.decrypt(params[:user][:old_password],User.decrypting_key)
      new_password = {
          :password=> AESCrypt.decrypt(params[:user][:password],User.decrypting_key),
          :password_confirmation=> AESCrypt.decrypt(params[:user][:password_confirmation],User.decrypting_key)
      }

      unless user
        return render json:{message:"User not found"}, status: 404
      end

      unless user.authenticate(old_password)
        return render json: {message: "Wrong password provided"}, status: 402
      end

      if is_password_confirmation_ok?
        user.update_attributes(new_password)
        render json:{message:"Successfully changed password"}, status: 200
      else
        render json:{message:"Not Authenticated"}, status: 401
      end
  end

  def reset_password
    if params[:user][:email].present?
      email = params[:user][:email]
      user = User.where(:email=> email).first
      if user
        begin
          UserMailer.reset_password(email).deliver
          render nothing: true, status: :ok
        rescue => ex
          logger.error ex.message
        end
      else
        render json: {message: "No user with that email address found"},status: 404
      end
    else
      render json: {message:"Email address is required"},status: 404
    end
    
  end

  private

  def user_params
    params.require(:user).permit(:id,:email,:password,:password_confirmation,
                                 :old_password,:name,:military_base_id,:lon,:lat,
                                 :driver,:zip_code,:street,:tel_number)
  end

  def ownership_filter
    unless current_user.id.to_i === params[:id].to_i
      render json: {message:"Unauthorize request"},status:401
      false
     end
  end

  def is_password_confirmation_ok?
    if params[:user][:password] === params[:user][:password_confirmation]
      true
    else
      false
    end
  end

  # def validates_and_save_regular_user_update_to_db
  #   if @user.update_attributes(user_params)
  #     respond_with @user, status: 200
  #   else
  #     render json: {message:@user.errors} , status: 404
  #   end
  # end
  #
  # def validates_and_save_premium_user_update_to_db
  #     if current_user.account && reject_if_any_missing_or_empty
  #       if @user.update_attributes(user_params)
  #         respond_with @user, status: 200
  #       else
  #         render json: {message:@user.errors} , status: 404
  #       end
  #     else
  #       render json: "Unauthorize request",status:401
  #     end
  # end
  #
  # def reject_if_any_missing_or_empty
  #   if params[:user][:house_number].present? &&
  #       params[:user][:street].present? &&
  #       params[:user][:zip_code].present?
  #     return true
  #   else
  #     return false
  #   end
  #end

end
