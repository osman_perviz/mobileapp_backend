BWFahrgemeinschaft::Application.routes.draw do

 # namespace :api,path:'/',defaults:{format:'json'},:constraints=>{:subdomain=>'api'} do
  namespace :api,defaults:{format:'json'} do
    namespace :v1 do

      resources :users , except: :new do
        member do
          put :change_password
        end
        collection do
          post :reset_password
        end
      end
      resources :military_bases,only:[:show,:index]
      resources :access,only:[:create,:destroy]
      match "ios_app_verifications/verify_receipt" => "ios_app_verifications#verify_receipt", via: :post
    end

  end

  resources :reset_password,only: [:new]

end
